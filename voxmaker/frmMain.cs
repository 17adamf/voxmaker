using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;
using System.Diagnostics;

namespace voxmaker
{
    public partial class frmMain : Form
    {
        List<string> folders = new List<string>();
        List<string> items = new List<string>();

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            populateFoldersList();
            cbfolders.SelectedIndex = 0;
            items = liboxSoundFiles.Items.Cast<String>().ToList();
        }


        private void liboxSoundFiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (liboxSoundFiles.SelectedIndex != -1)
            {
                liboxExportList.Items.Add(liboxSoundFiles.Items[liboxSoundFiles.SelectedIndex]);
            }
        }

        private void liboxExportList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (liboxExportList.SelectedIndex != -1)
            {
                liboxExportList.Items.RemoveAt(liboxExportList.SelectedIndex);
            }
        }


        private void export()
        {

            StreamWriter file = new StreamWriter(@"sounds\exportlist.txt");
            foreach (string line in liboxExportList.Items)
            {

                string temp = "file '" + line + "'";
                //temp = "file '" + temp +"'";
                file.WriteLine(temp);
            }
            file.Close();

            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            processStartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            processStartInfo.FileName = @"ffmpeg.exe";
            processStartInfo.Arguments = @"-f concat -safe 0 -i sounds\exportlist.txt -c copy sounds\exports\" + txtExportName.Text + ".wav -y";
            processStartInfo.CreateNoWindow = false;
            Process ffmpeg = Process.Start(processStartInfo);
            ffmpeg.WaitForExit();
            if (ffmpeg.ExitCode != 0)
            {
                MessageBox.Show("Error encountered while exporting", "Error Exporting", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dialog = MessageBox.Show("Export Successful\nView Exports Folder?", "Export Successful", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialog == DialogResult.Yes)
                {
                    ProcessStartInfo exportfolder = new ProcessStartInfo();
                    exportfolder.FileName = @"explorer.exe";
                    exportfolder.Arguments = @"sounds\exports\";
                    Process.Start(exportfolder);
                    
                }
            }

        }

        private void demoExport()
        {
            foreach (string file in liboxExportList.Items)
            {
                SoundPlayer soundItem = new SoundPlayer(file);
                soundItem.PlaySync();
            }
        }


        private void populateFoldersList()
        {
            string[] folders = Directory.GetDirectories(@"sounds\");
            foreach (string folder in folders)
            {
                cbfolders.Items.Add(folder);
            }

        }

        private void populateFileList()
        {
            liboxSoundFiles.Items.Clear();
            string[] filePaths = Directory.GetFiles(cbfolders.SelectedItem.ToString() + @"\", "*.wav");
            foreach (string file in filePaths)
            {
                liboxSoundFiles.Items.Add(file);
            }
            items = liboxSoundFiles.Items.Cast<String>().ToList();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (txtExportName.Text == string.Empty)
            {
                MessageBox.Show("You must enter a file name", "Required Field", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (txtExportName.Text.Contains(" "))
            {
                //shouldn't happen but just in case for now
                MessageBox.Show("No spaces in export name", "No Spaces", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                export();
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
          demoExport();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            liboxExportList.Items.Clear();
            txtExportName.Clear();
            txtSearch.Clear();
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public void btnMoveUp_Click(object sender, EventArgs e)
        {
            int selectedIndex = liboxExportList.SelectedIndex;
            if (selectedIndex > 0)
            {
                liboxExportList.Items.Insert(selectedIndex - 1, liboxExportList.Items[selectedIndex]);
                liboxExportList.Items.RemoveAt(selectedIndex + 1);
                liboxExportList.SelectedIndex = selectedIndex - 1;
            }
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            int selectedIndex = liboxExportList.SelectedIndex;
            if (selectedIndex < liboxExportList.Items.Count - 1 & selectedIndex != -1)
            {
                liboxExportList.Items.Insert(selectedIndex + 2, liboxExportList.Items[selectedIndex]);
                liboxExportList.Items.RemoveAt(selectedIndex);
                liboxExportList.SelectedIndex = selectedIndex + 1;

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (liboxExportList.SelectedIndex != -1)
            {
                liboxExportList.Items.RemoveAt(liboxExportList.SelectedIndex);

            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            liboxSoundFiles.Items.Clear();
            foreach (string line in items)
            {
                if (line.Contains(txtSearch.Text))
                {
                    liboxSoundFiles.Items.Add(line);
                }
            }
        }

        private void txtSearch_MouseClick(object sender, MouseEventArgs e)
        {
            txtSearch.Clear();
        }

        private void txtExportName_TextChanged(object sender, EventArgs e)
        {
            if (txtExportName.Text.Contains(" "))
            {
                txtExportName.Text = txtExportName.Text.Replace(' ', '_');
                txtExportName.SelectionStart = txtExportName.Text.Length;
                txtExportName.SelectionLength = 0;
            }
        }

        private void cbfolders_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateFileList();
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            liboxExportList.Items.Clear();
        }

        private void btnPreviewSelected_Click(object sender, EventArgs e)
        {
            SoundPlayer soundItem = new SoundPlayer(liboxSoundFiles.SelectedItem.ToString());
            soundItem.Play();
        }
    }
}

# voxmaker

A user interface to allow for the easy combining of WAV audio clips, intended for use with Half Life 1's audio files. 
Utilizes FFmpeg.




# How To Use
Build the project with Visual Studio. Uses .NET v4.6.1, but can very likely build using an older .NET if need be.

Once built, voxmaker checks for a folder next to it called "sounds". Inside of this folder, you can place folders of WAV files to pull from. In addition FFmpeg will need to be placed in the same folder as the voxmaker.exe once built.
Inside the root of the "sounds" folder, a file called "exportlist.txt" should be placed. It doesn't need to contain anything, but this is how FFmpeg is able to use the concat function across multiple files across multiple folders. 

Once exported, the file will be placed in the "sounds\exports" folder.



The folder structure should look something like this:

```
├───VoxmakerFolder 
│    ├───voxmaker.exe
│    ├───ffmpeg.exe
│    └───sounds
│          ├───exportlist.txt
│          ├───exports
│          └───examplefolder1
│                   └───example.wav   
```
 
